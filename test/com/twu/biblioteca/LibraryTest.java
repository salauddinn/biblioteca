package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LibraryTest {
    private Library library;
    ArrayList<Book> booklist;
    ArrayList<Movie> movieList;
    ArrayList<User> userList;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws FileNotFoundException {
        booklist = new ArrayList<>();
        movieList = new ArrayList<>();
        userList = new ArrayList<>();
        library = new Library(booklist, movieList, userList);
    }

    @Test
    public void checkOutBookshouldReturnExceptionSayingNotAvailableWhenBookIsAlreadyCheckedOut() throws MediaNotFoundException {
        Book clrs = mock(Book.class);
        booklist.add(clrs);
        when(clrs.isSame("clrs")).thenReturn(true);
        library.checkOutBook("clrs");
        thrown.expect(MediaNotFoundException.class);
        thrown.expectMessage("That book is not available");
        library.checkOutBook("clrs");
    }

    @Test
    public void checkOutBookshouldReturnBookDoesnotExistWhenBookIsNotInLibrary() throws MediaNotFoundException {
        thrown.expect(MediaNotFoundException.class);
        thrown.expectMessage("That book doesn't exit");
        library.checkOutBook("CLRS2");
    }

    @Test
    public void checkOutMovieShouldReturnExceptionSayingNotAvailableWhenMovieIsAlreadyCheckedOut() throws MediaNotFoundException {
        Movie Thani_Oruvan = mock(Movie.class);
        movieList.add(Thani_Oruvan);
        when(Thani_Oruvan.isSame("Thani Oruvan")).thenReturn(true);
        library.checkOutMovie("Thani Oruvan");
        thrown.expect(MediaNotFoundException.class);
        thrown.expectMessage("That movie already checked out");
        library.checkOutMovie("Thani Oruvan");
    }

    @Test
    public void checkOutMovieShouldReturnMovieDoesnotExistWhenMovieIsNotInLibrary() throws MediaNotFoundException {
        thrown.expect(MediaNotFoundException.class);
        thrown.expectMessage("That movie is not available");
        library.checkOutMovie("pokiri");
    }

    @Test
    public void returnBookshouldReturnThankYouWhenIReturnedCheckoutBook() throws MediaNotFoundException {
        User user = mock(User.class);
        userList.add(user);
        when(user.isSame("213-1235", "pwd")).thenReturn(true);
        library.login("213-1235", "pwd");
        Book clrs = mock(Book.class);
        booklist.add(clrs);
        when(clrs.isSame("clrs")).thenReturn(true);
        library.checkOutBook("clrs");
        assertEquals("Thank you for returning the book", library.returnBook("clrs"));
    }

    @Test
    public void returnBookshouldReturNBookDoesnotExistWhenBookIsNotInLibrary() {
        assertEquals("That is not a valid book to return", library.returnBook("CLRS"));
    }

    @Test
    public void shouldReturnBookIfBookIsCheckout() throws Exception {
        Book clrs = mock(Book.class);
        booklist.add(clrs);
        when(clrs.isSame("clrs")).thenReturn(true);
        assertTrue(library.checkOutBook("clrs").equals(clrs));
    }

    @Test
    public void shouldReturnExceptionWhenBookIsBookIsNotCheckout() throws Exception {
        thrown.expect(MediaNotFoundException.class);
        thrown.expectMessage("That book doesn't exit");
        library.checkOutBook("hello");
    }

    @Test
    public void loginShouldReturnUserWhenLibraryNameAndPasswordAreRight() {
        User user = mock(User.class);
        userList.add(user);
        when(user.isSame("213-1235", "pwd")).thenReturn(true);
        assertNotNull(library.login("213-1235", "pwd"));
    }

    @Test
    public void loginShouldReturNullWhenLibraryNameAndPasswordAreWrong() {
        User user = mock(User.class);
        userList.add(user);
        when(user.isSame("213-1235", "pwd")).thenReturn(false);
        assertNull(library.login("213-1235", "pwd"));
    }
}
package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class BibliotecaTest {
    private Biblioteca biblioteca;
    private InputHandler inputHandler;
    private OutputHandler outputHandler;
    private Menu menu;
    private Library library;

    @Before
    public void setUp() throws Exception {
        outputHandler = mock(OutputHandler.class);
        inputHandler = mock(InputHandler.class);
        menu = mock(Menu.class);
        library = mock(Library.class);
        biblioteca = new Biblioteca(inputHandler, outputHandler, menu, library);
    }

    @Test
    public void startShouldCallWelcomeMessageWhenAppIsAuthenticatesWithCorrectUserDetails() throws IOException {
        when(inputHandler.getName()).thenReturn("213-1235").thenReturn("pwd");
        when(library.login("213-1235", "pwd")).thenReturn(new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "8888888888"));
        when(inputHandler.getOption()).thenReturn(0);
        biblioteca.start();
        verify(outputHandler).display("Welcome to Bibilioteca");
    }

    @Test
    public void startShouldCallDisplayMentod() throws IOException {
        when(inputHandler.getName()).thenReturn("123-4567").thenReturn("a");
        when(library.login("123-4567", "a")).thenReturn(new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "8888888888"));
        when(inputHandler.getOption()).thenReturn(0);
        biblioteca.start();
        verify(outputHandler).display(menu.toString());
    }

    @Test
    public void startShouldCallPerformOperationMethodInMenuWithGivenOption() throws IOException {
        when(inputHandler.getName()).thenReturn("123-4567").thenReturn("a");
        when(library.login("123-4567", "a")).thenReturn(new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "8888888888"));
        when(inputHandler.getOption()).thenReturn(1).thenReturn(0);
        biblioteca.start();
        verify(outputHandler, atLeastOnce()).display("Welcome to Bibilioteca");
        verify(outputHandler, atLeastOnce()).display(menu.toString());
        verify(menu, atLeastOnce()).performOperation(1);
    }
}

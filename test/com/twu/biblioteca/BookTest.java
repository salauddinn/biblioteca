package com.twu.biblioteca;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class BookTest {
    @Test
    public void equalsShouldReturnTrueBothBooksAreSame() {
        Book book = new Book("CLRS Algorithms", "coreman", 2004);
        Book sameBook = new Book("CLRS Algorithms", "coreman", 2004);
        assertTrue(book.equals(sameBook));
    }

    @Test
    public void equalsShouldReturnFalseBothBooksAreNotSame() {
        Book clrs = new Book("CLRS Algorithms", "coreman", 2004);
        Book os = new Book("operating system", "galvin", 2004);
        assertFalse(clrs.equals(os));
    }

    @Test
    public void equalsShouldReturnFalseWhenStringIsComparedBook() {
        Book clrs = new Book("CLRS Algorithms", "coreman", 2004);
        assertFalse("book".equals(clrs));
    }

    @Test
    public void isSameNameShouldReturnTrueWhenBothNamesAreSame() {
        Book clrs = new Book("CLRS Algorithms", "coreman", 2004);
        Assert.assertTrue(clrs.isSame("CLRS Algorithms"));
    }

    @Test
    public void isSameNameShouldReturnFalseWhenBothNamesAreNotSame() {
        Book clrs = new Book("CLRS", "coreman", 2004);
        assertFalse(clrs.isSame("os"));
    }
}

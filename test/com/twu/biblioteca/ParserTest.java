package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

import static com.twu.biblioteca.BibliotecaApp.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ParserTest {
    private Parser parser;
    private BufferedReader bufferedReader;

    @Test
    public void parseShouldReturnSizezeroWhenTheBooksAreNotLibrary() throws IOException {

        parser = new Parser(bufferedReader, BOOK);
        when(bufferedReader.readLine()).thenReturn(null);
        List<Book> books = parser.parse();
        assertEquals(0, books.size());
    }

    @Before
    public void setUp() {
        bufferedReader = mock(BufferedReader.class);
    }

    @Test
    public void parseShouldReturnTrueWhenTheBooksThatAreInLibrary() throws IOException {
        parser = new Parser(bufferedReader, BOOK);
        when(bufferedReader.readLine()).thenReturn("CLRS Algorithms,coreman,2004").thenReturn(null);
        List<Book> books = parser.parse();
        assertTrue(books.get(0).isSame("CLRS Algorithms"));
    }

    @Test
    public void parseShouldReturnFalseWhenTheBookInLibrary() throws IOException {
        parser = new Parser(bufferedReader, BOOK);
        when(bufferedReader.readLine()).thenReturn("CLRS Algorithms,coreman,2004").thenReturn(null);
        List<Book> books = parser.parse();
        assertFalse(books.get(0).isSame("Head First"));
    }

    @Test
    public void parseShouldReturnNullWhenTheBooksThatAreNotInLibrary() throws IOException {
        parser = new Parser(bufferedReader, BOOK);
        when(bufferedReader.readLine()).thenReturn(null);
        List<Book> books = parser.parse();
        assertEquals(books, Collections.emptyList());
    }

    @Test
    public void parseShouldReturnSizezeroWhenTheMoviesAreNotLibrary() throws IOException {
        when(bufferedReader.readLine()).thenReturn(null);
        parser = new Parser(bufferedReader, MOVIE);
        List<Movie> movies = parser.parse();
        assertEquals(0, movies.size());
    }

    @Test
    public void parseShouldReturnTrueWhenTheMoviesThatAreInLibrary() throws IOException {
        parser = new Parser(bufferedReader, MOVIE);
        when(bufferedReader.readLine()).thenReturn("Thani Oruvan,2015,Mohan Raja,8.8").thenReturn(null);
        List<Movie> movies = parser.parse();
        assertEquals(1, movies.size());
        assertTrue(movies.get(0).isSame("Thani Oruvan"));
    }

    @Test
    public void parseShouldReturnNullWhenTheBooksThatAreNotInLibraryTyepisMovie() throws IOException {
        when(bufferedReader.readLine()).thenReturn(null);
        parser = new Parser(bufferedReader, MOVIE);
        List<Book> books = parser.parse();
        assertEquals(books, Collections.emptyList());
    }

    @Test
    public void parseShouldReturnSizezeroWhenNoUsersAreThere() throws IOException {
        parser = new Parser(bufferedReader, USER);
        when(bufferedReader.readLine()).thenReturn(null);
        List<User> users = parser.parse();
        assertEquals(0, users.size());
    }

    @Test
    public void parseShouldReturnTrueWhenTheUserHasIdAs213_1235() throws IOException {
        parser = new Parser(bufferedReader, USER);
        when(bufferedReader.readLine()).thenReturn("213-1235,pwd,myname,mygmail@gmail.com,888888888").thenReturn(null);
        List<User> users = parser.parse();
        assertTrue(users.get(0).isSame("213-1235", "pwd"));
    }

    @Test
    public void parseShouldReturnFalseWhenTheUserDoesnotHaveIdAs213_1245() throws IOException {
        parser = new Parser(bufferedReader, USER);
        when(bufferedReader.readLine()).thenReturn("213-1235,pwd,myname,mygmail@gmail.com,888888888").thenReturn(null);
        List<User> users = parser.parse();
        assertFalse(users.get(0).isSame("213-1245", "pwd"));
    }

    @Test
    public void parseShouldReturnFalseWhenTheUserDoesnHaveIdButWrongPassword() throws IOException {
        parser = new Parser(bufferedReader, USER);
        when(bufferedReader.readLine()).thenReturn("213-1235,pwd,myname,mygmail@gmail.com,888888888").thenReturn(null);
        List<User> users = parser.parse();
        assertFalse(users.get(0).isSame("213-1245", "pwd2"));
    }
 @Test
    public void parseShouldReturnEmptyListAndNotParseWhenParsedTypeAsAnotherObject() throws IOException {
     Object object=new Object();
        parser = new Parser(bufferedReader, object);
        when(bufferedReader.readLine()).thenReturn("213-1235,pwd,myname,mygmail@gmail.com,888888888").thenReturn(null);
        List<User> users = parser.parse();
        assertTrue(users.equals(Collections.emptyList()));
    }


}

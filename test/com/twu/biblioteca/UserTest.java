package com.twu.biblioteca;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class UserTest {
    @Test
    public void hasSameShouldReturnTrueWhentheLibrayNumeberAndPasswordAreSame() {
        User user = new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "888888888");
        assertTrue(user.isSame("213-1235", "pwd"));
    }

    @Test
    public void hasSameShouldReturnFalseWhentheLibrayNumeberAndPasswordAreNotSame() {
        User user = new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "888888888");
        assertFalse(user.isSame("123-4531", "pwe"));
    }

    @Test
    public void hasSameShouldReturnFalseWhentheLibrayNumeberIsSameButPasswordIsWrong() {
        User user = new User("213-1235", "pwd", "myname", "mygmail@gmail.com", "888888888");
        assertFalse(user.isSame("213-1235", "pwe"));
    }
}

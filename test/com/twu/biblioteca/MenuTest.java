package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MenuTest {
    MenuController menuController;
    private Menu menu;

    @Before
    public void setUp() {
        menuController = mock(MenuController.class);
        OutputHandler outputHandler = mock(OutputHandler.class);
        menu = new Menu(menuController, outputHandler);
    }

    @Test
    public void performOperstionShouldCallPeformExitOnMenuControllerhenOption0IsSelected() {
        menu.performOperation(0);
        verify(menuController).exit();
    }

    @Test
    public void performOperstionShouldCalllPerformDisplayBooksOnMenuControllerWhenOption1IsSelected() {
        menu.performOperation(1);
        verify(menuController).displayBooks();
    }

    @Test
    public void performOperstionShouldCalllPerformDisplayMoviesOnMenuControllerhenOption2IsSelected() {
        menu.performOperation(2);
        verify(menuController).displayMovies();
    }

    @Test
    public void performOperstionShouldCallPerformCeckoutBookOnMenuControllerWhenOption3IsSelected() throws MediaNotFoundException, IOException {
        menu.performOperation(3);
        verify(menuController).checkOutOfBook();
    }

    @Test
    public void performOperstionShouldCallPerformCeckoutMovieOnMenuControllerWhenOption4IsSelected() throws MediaNotFoundException, IOException {
        menu.performOperation(4);
        verify(menuController).checkOutOfMovie();
    }

    @Test
    public void performOperstionShouldCallPerformReturnBookOnMenuControllerWhenOption5IsSelected() throws IOException {
        menu.performOperation(5);
        verify(menuController).returnBook();
    }

    @Test
    public void performOperstionShouldCalldisplayUserOnMenuControllerWhenOption6IsSelected() throws IOException {
        menu.performOperation(6);
        verify(menuController).displayUser();
    }

    @Test
    public void performOperstionShouldCallInValidOnMenuControllerWhenOption7IsSelected() throws IOException {
        menu.performOperation(7);
        verify(menuController).inValid();
    }
}

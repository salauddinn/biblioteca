package com.twu.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class MenuControllerTest {
    private OutputHandler outputHandler;
    private InputHandler inputHandler;
    private Library library;
    private MenuController menuHandler;

    @Before
    public void setUp() {
        outputHandler = mock(OutputHandler.class);
        inputHandler = mock(InputHandler.class);
        library = mock(Library.class);
        menuHandler = new MenuController(inputHandler, outputHandler, library);
    }

    @Test
    public void performDisplayBooksShouldDisplayBooks() {
        menuHandler.displayBooks();
        verify(outputHandler).display("---------- List of Books ------------");
        verify(outputHandler).display(library.BookDetails());
    }

    @Test
    public void performDisplayMovieShouldDisplayMovie() {
        menuHandler.displayMovies();
        verify(outputHandler).display("---------- List of Movies ------------");
        verify(outputHandler).display(library.MovieDeatails());
    }

    @Test
    public void performCheckOutBookShouldReturnBook() throws MediaNotFoundException, IOException {
        when(inputHandler.getName()).thenReturn("CLRS");
        menuHandler.checkOutOfBook();
        verify(library, atLeastOnce()).checkOutBook("CLRS");
    }

    @Test
    public void performCheckOutMovieShouldReturnMovie() throws MediaNotFoundException, IOException {
        when(inputHandler.getName()).thenReturn("Saw");
        menuHandler.checkOutOfMovie();
        verify(library, atLeastOnce()).checkOutMovie("Saw");
    }

    @Test
    public void performOperstionShouldCallReturnBook() throws IOException {
        when(inputHandler.getName()).thenReturn("CLRS");
        menuHandler.returnBook();
        verify(library, atLeastOnce()).returnBook("CLRS");
    }

    @Test
    public void performOperstionShouldCallUserDetails() throws IOException {
        when(inputHandler.getName()).thenReturn("CLRS");
        menuHandler.displayUser();
        verify(outputHandler).display(library.currentUserDetails());
    }
}

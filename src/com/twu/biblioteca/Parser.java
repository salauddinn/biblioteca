package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static com.twu.biblioteca.BibliotecaApp.*;

class Parser {
    private final BufferedReader reader;
    private final Object parserType;

    Parser(BufferedReader reader, Object parserType) {
        this.reader = reader;
        this.parserType = parserType;
    }

    List parse() {
        List parserList = new ArrayList<>();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                StringTokenizer tokens = new StringTokenizer(line, ",");
                insertParseValue(parserType, parserList, tokens);
            }
        } catch (IOException e) {
            return parserList;
        }
        return parserList;
    }

    private void insertParseValue(Object parserType, List parserList, StringTokenizer list) {
        if (parserType == BOOK) {
            parserList.add(new Book(list.nextToken(), list.nextToken(), Integer.parseInt(list.nextToken())));
        } else if (parserType == MOVIE) {
            parserList.add(new Movie(list.nextToken(), Integer.parseInt(list.nextToken()), list.nextToken(), list.nextToken()));
        } else if (parserType == USER) {
            parserList.add(new User(list.nextToken(), list.nextToken(), list.nextToken(), list.nextToken(), list.nextToken()));
        }
    }
}

package com.twu.biblioteca;

import java.io.IOException;

//understands  the start of an application
class Biblioteca {
    private InputHandler inputHandler;
    private OutputHandler outputHandler;
    private Menu menu;
    private Library library;

    Biblioteca(InputHandler inputHandler, OutputHandler outputHandler, Menu menu, Library library) {
        this.inputHandler = inputHandler;
        this.outputHandler = outputHandler;
        this.menu = menu;
        this.library = library;
    }

    void start() {
        try {
            if (userLogin())
                return;
            outputHandler.display("Welcome to Bibilioteca");
            int option = -1;
            while (option != 0) {
                outputHandler.display(menu.toString());
                option = inputHandler.getOption();
                menu.performOperation(option);
            }

        } catch (Exception e) {
            outputHandler.display(e.getMessage());
        }
    }

    private boolean userLogin() throws IOException {
        outputHandler.display("Enter The LibrayNumber");
        String libraryNumber = inputHandler.getName();
        outputHandler.display("Enter Password");
        String password = inputHandler.getName();
        User user = library.login(libraryNumber, password);
        if (user == null) {
            outputHandler.display("Enter valid details");
            start();
            return true;
        }
        return false;
    }
}

package com.twu.biblioteca;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//understands the colection of books and movies
class Library {
    private final List<Book> booksList;
    private Map<Book, User> checkoutBookList;
    private List<Movie> movieList;
    private Map<Movie, User> checkoutMovieList;
    private List<User> users;
    private User currentUser;

    Library(List<Book> booksList, List<Movie> movieList, List<User> users) {
        this.booksList = booksList;
        this.users = users;
        checkoutBookList = new HashMap<>();
        checkoutMovieList = new HashMap<>();
        this.movieList = movieList;
    }

    public static <E> void printArray(E[] elements) {
        for (E element : elements) {
            System.out.println(element);
        }
        System.out.println();
    }

    Book checkOutBook(String name) throws MediaNotFoundException {
        if (isCheckedOutBook(name)) {
            throw MediaNotFoundException.bookNotFound("That book is not available");
        }
        for (Book book : booksList) {
            if (book.isSame(name)) {
                checkoutBookList.put(book, currentUser);
                booksList.remove(book);
                return book;
            }
        }
        throw MediaNotFoundException.bookNotFound("That book doesn't exit");
    }

    Movie checkOutMovie(String name) throws MediaNotFoundException {
        if (isCheckedOutMovie(name)) {
            throw MediaNotFoundException.movieNotFound("That movie already checked out");
        }
        for (Movie movie : movieList) {
            if (movie.isSame(name)) {
                checkoutMovieList.put(movie, currentUser);
                movieList.remove(movie);
                return movie;
            }

        }
        throw MediaNotFoundException.movieNotFound("That movie is not available");

    }

    String returnBook(String name) {
        for (Map.Entry<Book, User> entry : checkoutBookList.entrySet()) {
            if (entry.getValue().equals(currentUser)) {
                if (entry.getKey().isSame(name)) {
                    checkoutBookList.remove(entry.getKey());
                    booksList.add(entry.getKey());
                    return "Thank you for returning the book";
                }
            }
        }
        return "That is not a valid book to return";
    }

    User login(String libraryName, String password) {
        for (User user : users) {
            if (user.isSame(libraryName, password)) {
                this.currentUser = user;
                return user;
            }
        }
        return currentUser;
    }

    String currentUserDetails() {
        return currentUser.toString();
    }

    String BookDetails() {
        String string = "";
        for (Book book : booksList) {
            string += book + "\n";
        }
        return string;
    }

    String MovieDeatails() {
        String string = "";
        for (Movie book : movieList) {
            string += book + "\n";
        }
        return string;
    }


    private boolean isCheckedOutBook(String name) {
        for (Book book : checkoutBookList.keySet()) {
            if (book.isSame(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean isCheckedOutMovie(String name) {
        for (Movie movie : checkoutMovieList.keySet()) {
            if (movie.isSame(name)) {
                return true;
            }
        }
        return false;
    }
}

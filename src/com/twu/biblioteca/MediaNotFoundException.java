package com.twu.biblioteca;

//understands the invalid type
class MediaNotFoundException extends Exception {
    private MediaNotFoundException(String message) {
        super(message);
    }

    static MediaNotFoundException bookNotFound(String message) throws MediaNotFoundException {
        return new MediaNotFoundException(message);
    }

    static MediaNotFoundException movieNotFound(String message) throws MediaNotFoundException {
        return new MediaNotFoundException(message);
    }
}

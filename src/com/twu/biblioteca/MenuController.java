package com.twu.biblioteca;

import java.io.IOException;

//understands perform the action based on commands
class MenuController {
    private OutputHandler outputHandler;
    private InputHandler inputHandler;
    private Library library;

    MenuController(InputHandler inputHandler, OutputHandler outputHandler, Library library) {
        this.outputHandler = outputHandler;
        this.inputHandler = inputHandler;
        this.library = library;
    }

    void displayBooks() {
        outputHandler.display("---------- List of Books ------------");
        outputHandler.display(library.BookDetails());
    }

    void returnBook() throws IOException {
        String bookName = inputHandler.getName();
        String returnMessage = library.returnBook(bookName);
        outputHandler.display(returnMessage);
    }

    void checkOutOfBook() throws IOException, MediaNotFoundException {
        String bookName = inputHandler.getName();
        Book book = library.checkOutBook(bookName);
        outputHandler.display("Thank you! Enjoy the book");

    }

    void exit() {
        outputHandler.display("Thank you... Have a Nice Day");
    }

    void inValid() {
        outputHandler.display("Type Valid Option");
    }


    void checkOutOfMovie() throws IOException, MediaNotFoundException {
        String movieName = inputHandler.getName();
        Movie movie = library.checkOutMovie(movieName);
        outputHandler.display("Thank you! Enjoy the Movie");
    }

    void displayMovies() {
        outputHandler.display("---------- List of Movies ------------");
        outputHandler.display(library.MovieDeatails());
    }


    void displayUser() {
        outputHandler.display(library.currentUserDetails());
    }
}
package com.twu.biblioteca;

//understands the information about product
class Book {
    private String name;
    private String author;
    private int year;

    Book(String name, String author, int year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }

    boolean isSame(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        if (year != book.year) return false;
        if (!name.equals(book.name)) return false;
        return author.equals(book.author);

    }

    @Override
    public String toString() {
        return String.format("%-30s %-30s %-10d %s", name, author, year, name);
    }
}

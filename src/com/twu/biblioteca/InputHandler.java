package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.IOException;

//understands user data
class InputHandler {
    private BufferedReader input;

    InputHandler(BufferedReader input) {
        this.input = input;
    }

    int getOption() throws IOException {

        return Integer.parseInt(input.readLine());
    }

    String getName() throws IOException {
        return input.readLine().trim();
    }


}

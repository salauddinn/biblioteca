package com.twu.biblioteca;

//understands a person who uses library
class User {
    private String libraryNumber;
    private String password;
    private String name;
    private String email;
    private String phone;

    User(String libraryNumber, String password, String name, String email, String phone) {
        this.libraryNumber = libraryNumber;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return String.format("%-15s %-15s %-15s %15s", name, libraryNumber, email, phone);
    }

    boolean isSame(String libraryName, String password) {
        return this.libraryNumber.equals(libraryName) && this.password.equals(password);
    }
}

package com.twu.biblioteca;

//understands list of commands
class Menu {
    private final MenuController menuController;
    private final OutputHandler outputHandler;

    Menu(MenuController menuController, OutputHandler outputHandler) {
        this.menuController = menuController;
        this.outputHandler = outputHandler;
    }

    void performOperation(int option) {
        try {
            switch (option) {
                case 0: {
                    menuController.exit();
                    break;
                }
                case 1: {
                    menuController.displayBooks();
                    break;
                }
                case 2: {
                    menuController.displayMovies();

                    break;
                }
                case 3: {
                    menuController.checkOutOfBook();
                    break;
                }
                case 4: {
                    menuController.checkOutOfMovie();
                    break;
                }
                case 5: {
                    menuController.returnBook();
                    break;
                }
                case 6: {
                    menuController.displayUser();
                    break;
                }
                default: {
                    menuController.inValid();
                }

            }
        } catch (MediaNotFoundException e) {
            outputHandler.display(e.getMessage());
        } catch (Exception e) {
            outputHandler.display("Type Invalid Inputs..");
        }
    }


    @Override
    public String toString() {
        return "Choose one Option\n" +
                "0.Exit\n" +
                "1.List of Books\n" +
                "2.List of Movies\n" +
                "3.Checkout Book\n" +
                "4.Checkout Movie\n" +
                "5.Return Book\n" +
                "6.My Profile\n" +
                ":";
    }
}
package com.twu.biblioteca;

// understands  information about cinema
class Movie {
    private String name;
    private int year;
    private String director;
    private String rating;

    Movie(String name, int year, String director, String rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = rating;
    }

    boolean isSame(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        if (year != movie.year) return false;
        if (name != null ? !name.equals(movie.name) : movie.name != null) return false;
        if (director != null ? !director.equals(movie.director) : movie.director != null) return false;
        return rating != null ? rating.equals(movie.rating) : movie.rating == null;
    }

    @Override
    public String toString() {
        return String.format("%-20s %-10s %-20s %-5s", name, year, director, rating);
    }
}

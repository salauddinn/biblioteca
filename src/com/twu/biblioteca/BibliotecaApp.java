package com.twu.biblioteca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

//understands the application
public class BibliotecaApp {
    public static final Object USER = new Object();
    public static final Object BOOK = new Object();
    public static final Object MOVIE = new Object();
    public static final String LIBRARY_BOOKS_FILE = System.getProperty("user.dir") + "/Resources/books.txt";
    public static final String LIBRARY_MOVIE_FILE = System.getProperty("user.dir") + "/Resources/movies.txt";
    public static final String LIBRARY_USERS_FILE = System.getProperty("user.dir") + "/Resources/users.txt";

    public static void main(String args[]) {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        OutputHandler outputHandler = new OutputHandler();
        InputHandler inputHandler = new InputHandler(input);
        Library library = null;
        try {
            Parser boookparser = new Parser(new BufferedReader(new FileReader(LIBRARY_BOOKS_FILE)), BOOK);
            Parser movieParser = new Parser(new BufferedReader(new FileReader(LIBRARY_MOVIE_FILE)), MOVIE);
            Parser userParser = new Parser(new BufferedReader(new FileReader(LIBRARY_USERS_FILE)), USER);
            library = new Library(boookparser.parse(), movieParser.parse(), userParser.parse());
        } catch (Exception e) {
            System.out.println("dd" + e.getMessage());
        }
        MenuController menuHandler = new MenuController(inputHandler, outputHandler, library);
        Menu menu = new Menu(menuHandler, outputHandler);
        Biblioteca biblioteca = new Biblioteca(inputHandler, outputHandler, menu, library);
        biblioteca.start();
    }
}
